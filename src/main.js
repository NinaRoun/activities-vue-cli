import Vue from 'vue'
import VueRouter from 'vue-router'
import App from './App.vue'
import Statistics from './Statistics.vue'
import Home from './Home.vue'
import axios from 'axios'
import VueAxios from 'vue-axios'
import store from './store'

Vue.use(VueRouter);
Vue.use(VueAxios, axios);

const routes = [
	{ path: '/statistics', component: Statistics },
	{ path: '/', component: Home }
];

const router = new VueRouter({
	routes,
	mode: 'history'
});

new Vue({
  	el: '#app',
  	router,
  	store,
  	render: h => h(App)
})
