import Vue from 'vue'
import Vuex from 'vuex'


Vue.use(Vuex);

export default new Vuex.Store({
	state: {
		activities: [],
		currentTab: 0,
	},
	getters: {
		activities: function(state){
			//console.log('getting activities in store', state.activities, state);
			return state.activities
		},
		currentTab: function(state) {
			console.log('getting current tab from store');
			return state.currentTab;
		}
	},
	mutations: {
		addActivity: function(state, data){
			state.activities.push({
				name: data.activity, 
				type: data.type, 
				participants: data.participants, 
				price: data.price
			})
		},
		deleteAllActivities: function(state, data){
			state.activities = [];
		},
		setCurrentTab: function(state, index){
			console.log('mutating current tab in store');
			state.currentTab = index;
		}
	},
	actions: {
		addActivity: function(context, data){
			//console.log('adding activity in store');
			context.commit('addActivity', data)
		},
		deleteAllActivities: function(context, data){
			context.commit('deleteAllActivities', data);
		},
		setCurrentTab: function(context, index) {
			console.log('setting current tab in store');
			context.commit('setCurrentTab', index);
		}
	}
})

