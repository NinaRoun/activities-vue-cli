# Activity List and Statistics

> A Vue.js project

Build Setup

# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

The project structure:

```
├── webpack.config.js 			# manages build steps
├── package.json 				# dependencies
├── .gitignore 					# ignores node_modules
├── index.html 					# where built files will be auto injected
└── src/
	├── App.vue 				# the root component that Home.vue and Statistics.vue are nested within
	├── Home.vue 				# a building block #1
	├── Statistics.vue 			# a building block #2
	├── main.js 				# renders App.vue and mounts to the DOM
	└── store.js 				# for Vuex 
```
